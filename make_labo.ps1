$labohome = (gl).Path

# Step 1, installo libs/users e libs/settings
cd $labohome\libs\users
.\make_venv_win.ps1

cd $labohome\libs\settings
.\make_venv_win.ps1

# Step 2, installo libs/gui
cd $labohome\libs\gui
.\make_venv_win.ps1

.venv/Scripts/Activate.ps1
py -m setup build_res
deactivate

# Step 3, setup Qube
cd $labohome\apps\Qube
.\make_venv_win.ps1

# Step 4, setup Seam
cd $labohome\apps\LaboSeam
.\make_venv_win.ps1

.venv\Scripts\Activate.ps1
py -m setup build_res
deactivate

cd $labohome
